package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 128)
	private String address_city;
	
	@Column(nullable = false, length = 128)
	private String address_street;
	
	public UserProfile() {
	}
	
	public UserProfile(String address_city, String address_street) {
		this.address_city = address_city;
		this.address_street = address_street;
	}
	
	public void setAddress_city(String address_city) {
		this.address_city = address_city;
	}
	
	public void setAddress_street(String address_street) {
		this.address_street = address_street;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getAddress_city() {
		return address_city;
	}
	
	public String getAddress_street() {
		return address_street;
	}
}

