package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class CompanyProfile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 128)
	private String city;
	
	@Column(nullable = false, length = 128)
	private String street;
	
	public CompanyProfile() {
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getStreet() {
		return street;
	}
	
	public CompanyProfile(String city, String street) {
		this.city = city;
		this.street = street;
	}
}
