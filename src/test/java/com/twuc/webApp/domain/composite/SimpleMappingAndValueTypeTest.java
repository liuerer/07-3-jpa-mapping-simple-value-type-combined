package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMappingAndValueTypeTest extends JpaTestBase {
	
	@Autowired
	private UserProfileRepository userProfileRepo;
	
	@Autowired
	private CompanyProfileRepository companyProfileRepo;
	
	@Test
	void should_get_and_save_user_profile_entity() {
		ClosureValue<Long> expectedId = new ClosureValue<>();
		
		flushAndClear(em -> {
			final UserProfile profile =
				userProfileRepo.save(new UserProfile("xi'an", "chang_an_street"));
			expectedId.setValue(profile.getId());
		});
		
		run(em -> {
			final UserProfile profile =
				userProfileRepo.findById(expectedId.getValue()).orElseThrow(
					RuntimeException::new);
			
			assertEquals("xi'an", profile.getAddress_city());
			assertEquals("chang_an_street", profile.getAddress_street());
		});
	}
	
	@Test
	void should_get_and_save_company_profile_entity() {
		ClosureValue<Long> expectedId = new ClosureValue<>();
		
		flushAndClear(em -> {
			final CompanyProfile profile =
				companyProfileRepo.save(new CompanyProfile("xi'an", "chang_an_street"));
			expectedId.setValue(profile.getId());
		});
		
		run(em -> {
			final CompanyProfile profile =
				companyProfileRepo.findById(expectedId.getValue()).orElseThrow(
					RuntimeException::new);
			
			assertEquals("xi'an", profile.getCity());
			assertEquals("chang_an_street", profile.getStreet());
		});
	}
}
